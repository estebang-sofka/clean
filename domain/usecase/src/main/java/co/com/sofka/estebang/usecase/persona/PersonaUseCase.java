package co.com.sofka.estebang.usecase.persona;

import co.com.sofka.estebang.model.persona.Persona;
import co.com.sofka.estebang.model.persona.gateways.PersonaRepository;
import lombok.RequiredArgsConstructor;
import reactor.core.publisher.Mono;

@RequiredArgsConstructor
public class PersonaUseCase {

    private final PersonaRepository personaRepository;

    public Mono<Persona> getPerson(String id) {
        return personaRepository.getPerson(id);
    }

    public Mono<Float> getBalanceAccount(String id) {
        return personaRepository.getBalanceAccount(id);
    }

}
