package co.com.sofka.estebang.model.persona;

import lombok.Builder;
import lombok.Data;

@Data
@Builder(toBuilder = true)
public class Persona {
    private String id;
    private String name;
    private Float balanceAccount;
}
