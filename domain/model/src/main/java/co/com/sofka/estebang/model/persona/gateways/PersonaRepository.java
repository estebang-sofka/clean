package co.com.sofka.estebang.model.persona.gateways;

import co.com.sofka.estebang.model.persona.Persona;
import reactor.core.publisher.Mono;

public interface PersonaRepository {
    Mono<Persona> getPerson(String id);

    Mono<Float> getBalanceAccount(String id);
}
