package co.com.sofka.estebang.mongo;

import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.data.repository.query.ReactiveQueryByExampleExecutor;

public interface MongoDBRepository extends ReactiveMongoRepository<PersonaData, String>, ReactiveQueryByExampleExecutor<PersonaData> {
}
