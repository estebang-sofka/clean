package co.com.sofka.estebang.mongo;

import org.springframework.data.mongodb.core.mapping.Document;

@Document("Persona")
public class PersonaData {
    private String id;
    private String name;
    private Float balanceAccount;
}
