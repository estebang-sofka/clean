package co.com.sofka.estebang.graphqlapi;

import co.com.sofka.estebang.usecase.persona.PersonaUseCase;
import graphql.kickstart.tools.GraphQLQueryResolver;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import reactor.core.publisher.Mono;

@RequiredArgsConstructor
@Controller
/**
 * To interact with the API make use of Playground in the "/playground" path, but remember,
 * Playground ONLY must be used in dev or qa environments, not in production.
 */
public class ApiQueries implements GraphQLQueryResolver {

    private final PersonaUseCase useCase;

    public Mono<Float> getSomething(String objRequest/* change for object request */) {
        return useCase.getBalanceAccount(objRequest);
        //return "Hello world from graphql-api queries " + objRequest;
    }
}