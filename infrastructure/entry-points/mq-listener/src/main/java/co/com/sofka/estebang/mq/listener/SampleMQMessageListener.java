package co.com.sofka.estebang.mq.listener;

import co.com.bancolombia.commons.jms.mq.MQListener;
import co.com.sofka.estebang.usecase.persona.PersonaUseCase;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Mono;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.TextMessage;

@Component
@AllArgsConstructor
public class SampleMQMessageListener {
    private final PersonaUseCase useCase;

    // For fixed queues
    @MQListener
    public Mono<Float> process(Message message) throws JMSException {
        String text = ((TextMessage) message).getText();
        return useCase.getBalanceAccount(text);
        //return Mono.empty();
    }

    // For an automatic generated temporary queue
    // @MQListener(tempQueueAlias = "any-custom-value")
    // public Mono<Void> processFromTemporaryQueue(Message message) throws JMSException {
    //     String text = ((TextMessage) message).getText();
    //     // return useCase.sample(text);
    //     return Mono.empty();
    // }
}
