package co.com.sofka.estebang.api;

import co.com.sofka.estebang.model.persona.Persona;
import co.com.sofka.estebang.usecase.persona.PersonaUseCase;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.server.ServerRequest;
import org.springframework.web.reactive.function.server.ServerResponse;
import reactor.core.publisher.Mono;

@Component
@RequiredArgsConstructor
public class Handler {
    private final PersonaUseCase personaUseCase;

    //private  final UseCase2 useCase2;
    public Mono<ServerResponse> getPerson(ServerRequest serverRequest) {
        return personaUseCase.getPerson(serverRequest.pathVariable("id"))
                .flatMap(persona -> ServerResponse.ok().body(persona, Persona.class));
        //return ServerResponse.ok().body("", String.class);
    }

    public Mono<ServerResponse> getBalanceAccount(ServerRequest serverRequest) {
        return personaUseCase.getBalanceAccount(serverRequest.pathVariable("id"))
                .flatMap(balanceAccount -> ServerResponse.ok().body(balanceAccount, Float.class));
        //return ServerResponse.ok().body("", String.class);
    }

    public Mono<ServerResponse> listenPOSTUseCase(ServerRequest serverRequest) {
        // usecase.logic();
        return ServerResponse.ok().body("", String.class);
    }
}
