package co.com.sofka.estebang.config;

import co.com.sofka.estebang.model.persona.gateways.PersonaRepository;
import co.com.sofka.estebang.usecase.persona.PersonaUseCase;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.FilterType;

@Configuration
@ComponentScan(basePackages = "co.com.sofka.estebang.usecase",
        includeFilters = {
                @ComponentScan.Filter(type = FilterType.REGEX, pattern = "^.+UseCase$")
        },
        useDefaultFilters = false)
public class UseCasesConfig {
    //@Bean
//    public PersonaUseCase personaUseCase(PersonaRepository personaRepository) {
//        return new PersonaUseCase(personaRepository);
//    }
}
